#!/bin/sh
echo "### Stopping Plex"
docker stop plex
docker rm plex
echo "### Cleaning Images"
docker image prune -af
sleep 2