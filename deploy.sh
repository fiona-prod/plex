#!/bin/sh -x

docker pull plexinc/pms-docker:plexpass

docker create \
  --name=plex \
  --net=host \
  -e NVIDIA_VISIBLE_DEVICES=all \
  -e PLEX_UID=10101 \
  -e PLEX_GID=10101 \
  -e VERSION=docker \
  -e TZ=America/Chicago \
  -e PLEX_CLAIM=${plex_claim} \
  -e ADVERTISE_IP=${advertise_ip} \
  -v /docker/plex/config:/config \
  -v /storage/shows:/data/tvshows \
  -v /storage/movies:/data/movies \
  -v /storage/music:/data/music \
  -v /tmp/plex:/transcode \
  --memory=16G \
  --restart unless-stopped \
  --gpus all \
  --device=/dev/dri:/dev/dri \
  -e NVIDIA_DRIVER_CAPABILITIES=compute,video,utility \
  plexinc/pms-docker:plexpass

docker start plex
echo "### Deployment done"